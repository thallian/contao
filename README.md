[Contao](https://contao.org/) is a CMS for medium to large websites.

Uses MariaDb or MySQL.

After starting the image the first time you need to setup an admin account.
For this access `/contao/install.php`.

# Volumes
- `/var/lib/contao/files`
- `/var/lib/contao/templates`
- `/var/lib/contao/system/modules`

# Environment Variables
## DOMAIN
Domain on which this intallation is reachable.

## CONTAO_DB_HOST
Database host.

## CONTAO_DB_NAME
- default: contao

Database name.

## CONTAO_DB_USER
- default: contao

Database user.

## CONTAO_DB_PASSWORD
Password for the database user.

## CONTAO_INSTALL_PASSWORD
Password to access the install section.

## CONTAO_DB_PORT
- default: 3306

Database port.

## CONTAO_SECRET
A random 32-char string, used for secret encryption.

# Ports
- 80

# Capabilities
- CHOWN
- FOWNER
- DAC_OVERRIDE
- NET_BIND_SERVICE
- SETGID
- SETUID
