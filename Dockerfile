FROM quay.io/thallian/php7-fpm:latest

ENV FPMUSER nginx
ENV FPMGROUP nginx

ADD /rootfs /

RUN apk add --no-cache \
    nginx \
    libressl \
    tar \
    wget \
    ca-certificates \
    php7 \
    php7-json \
    php7-phar \
    php7-iconv \
    php7-mbstring \
    php7-openssl \
    php7-zlib \
    php7-gd \
    php7-dom \
    php7-dom \
    php7-pdo \
    php7-mcrypt \
    php7-mysqli \
    php7-xml \
    php7-curl \
    php7-ctype \
    php7-session \
    php7-soap \
    php7-zip \
    mariadb-client

RUN addgroup -g 2222  access
RUN addgroup nginx access

RUN rm /etc/nginx/conf.d/default.conf

RUN mkdir /var/lib/contao
RUN wget -qO- https://download.contao.org/lts | tar -xz -C /var/lib/contao --strip 1

ENV HOME /var/lib/contao
WORKDIR /var/lib/contao

RUN chown -R nginx:nginx /var/lib/contao

RUN mkdir /run/nginx

EXPOSE 80

VOLUME /var/lib/contao/files
